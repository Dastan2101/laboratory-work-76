const express = require('express');
const nanoid = require('nanoid');
const router = express.Router();
const fileMessages = require('../database');

router.post('/', (req, res) => {

    if (req.body.author === '' || req.body.message === '') {
        res.status(400).send({error: "Author and message must be present in the request"});

    } else {

        const date = new Date().toISOString();
        req.body.id = nanoid();
        req.body.datetime = date;

        fileMessages.addMessage(req.body);
        res.send({message: "Message has been sent"})

    }

});

router.get('/', (req, res) => {

    if (req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())) {
            res.status(400).send({error: "Uncorrected date"})
        } else {

            fileMessages.getMessage().map(index => {
                if (index.datetime === date.toISOString()) {
                    res.send(fileMessages.getNewMassage(date.toISOString()));
                }

            })

        }
    } else {
        res.send(fileMessages.getMessage());

    }

});


module.exports = router;