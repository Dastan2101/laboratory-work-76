const fs = require('fs');

const messageFile = './messages.json';

let data = [];


module.exports = {
    init() {
        try {
            const fileContent = fs.readFileSync(messageFile);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = []
        }
    },
    getMessage() {

        return data.slice(-30);

    },
    getNewMassage(date) {

        return data.filter(obj => obj.datetime > date);

    },
    addMessage(mesObj) {
        data.push(mesObj);
        this.save()
    },
    save() {
        fs.writeFileSync(messageFile, JSON.stringify(data, null, 2))
    }
};