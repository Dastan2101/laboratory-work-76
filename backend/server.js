const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const app = express();
const fileMessages = require('./database');
fileMessages.init();

app.use(express.json());
app.use(cors());

const port = 7555;

app.use('/messages', messages);

app.listen(port);