import {
    CHANGE_HANDLER,
    CREATE_MESSAGE_ERROR,
    CREATE_MESSAGE_SUCCESS,
    INITIAL_MESSAGES_SUCCESS, NEW_MESSAGE
} from "../actions/actionTypes";

const initialState = {
    messages: [],
    author: '',
    message: '',
    request: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIAL_MESSAGES_SUCCESS:

            return {
                ...state, messages: action.messages
            };

        case CHANGE_HANDLER:
            return {
                ...state,
                [action.input.type]: action.input.value
            };

        case CREATE_MESSAGE_SUCCESS:
            return {
                ...state, request: action.answer
            };
        case NEW_MESSAGE:
            return {
                ...state, messages: [...state.messages].concat(action.data)
            };
        case CREATE_MESSAGE_ERROR:
            return {
                ...state, request: action.error.response.data
            };

        default:
            return state;
    }
};

export default reducer