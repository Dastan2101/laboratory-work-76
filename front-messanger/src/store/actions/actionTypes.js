import axios from '../../axios-messanger';

export const INITIAL_MESSAGES_SUCCESS = 'INITIAL_MESSAGES_SUCCESS';

export const CHANGE_HANDLER = 'CHANGE_HANDLER';

export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const CREATE_MESSAGE_ERROR = 'CREATE_MESSAGE_ERROR';

export const NEW_MESSAGE = 'NEW_MESSAGE';

export const initialMessagesSuccess = messages => ({type: INITIAL_MESSAGES_SUCCESS, messages});

export const changeInputHandler = input => ({type: CHANGE_HANDLER, input});

export const createMessageSuccess = (answer) => ({type: CREATE_MESSAGE_SUCCESS, answer});
export const createMessageError = (error) => ({type: CREATE_MESSAGE_ERROR, error});


export const getNewMessage = (data) => ({type: NEW_MESSAGE, data});

export const initMessages = () => {
    return dispatch => {
        return axios.get('/messages').then(
            response => dispatch(initialMessagesSuccess(response.data))
        )
    }
};

export const createMessage = (message) => {
    return dispatch => {
        return axios.post('/messages', message).then(
            response => {
                dispatch(createMessageSuccess(response.data))
            }, error => dispatch(createMessageError(error))
        )
    }
};

export const gettingNewMessage = date => {
    return dispatch => {
        return axios.get('messages?datetime=' + date).then(
            response => dispatch(getNewMessage(response.data))
        )
    }
};