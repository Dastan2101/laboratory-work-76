import React from 'react';
import './Message.css'

const Message = (props) => {

    return (
        props.messages.map((message, key) => {

            let time = message.datetime.slice(11, -5);

            return (
                <div className="message-block" key={key}>
                    <span className="author">{message.author}</span>
                    <span className="message">{message.message}</span>
                    <span className="date-time">{time}</span>
                </div>
            )
        })
    );
};

export default Message;