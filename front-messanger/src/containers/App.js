import React, {Component, Fragment} from 'react';
import './App.css';
import Message from "../components/Message/Message";
import AddMessageForm from "../components/AddMessageForm/AddMessageForm";
import {
    changeInputHandler,
    createMessage,
    gettingNewMessage,
    initMessages
} from "../store/actions/actionTypes";
import {connect} from "react-redux";
import {NotificationContainer, NotificationManager} from "react-notifications";

let interval;

class App extends Component {

    componentDidMount() {
        this.props.initialMessages()
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    componentDidUpdate(prevProps) {

        if (prevProps.messages.length !== this.props.messages.length) {
            this.props.initialMessages()

        }

        let date = this.props.messages[this.props.messages.length - 1].datetime;

        clearInterval(interval);


        interval = setInterval(() => {

            this.props.gettingNewMessage(date);
            this.props.initialMessages();
        }, 5000)

    }

    changeHandler = event => {
        event.preventDefault();
        const input = {
            type: event.target.name,
            value: event.target.value
        };
        this.props.changeInputHandler(input)
    };

    sendMessage = () => {

        const message = {
            author: this.props.author,
            message: this.props.message
        };

        this.props.createMessage(message);

        setTimeout(() => {
            if (this.props.request) {
                if (this.props.request.message) {
                    return NotificationManager.success(this.props.request.message, 'Success', 2000);

                }
                if (this.props.request.error) {
                    return NotificationManager.error(this.props.request.error, 'Error', 2000)

                }

            }
        }, 1000)


    };


    render() {
        return (
            <Fragment>
                <NotificationContainer/>

                <div className="App">
                    <AddMessageForm
                        addMessage={() => this.sendMessage()}
                        onChange={(e) => this.changeHandler(e)}
                    />

                    <Message
                        messages={this.props.messages ? this.props.messages : []}
                    />
                </div>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages,
    author: state.author,
    message: state.message,
    request: state.request
});

const mapDispatchToProps = dispatch => ({
    initialMessages: () => dispatch(initMessages()),
    changeInputHandler: input => dispatch(changeInputHandler(input)),
    createMessage: message => dispatch(createMessage(message)),
    gettingNewMessage: data => dispatch(gettingNewMessage(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
